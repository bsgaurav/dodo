/* TODO
TO ADD
allow for indented items/notes on items
edit todos in a buffer
allow for single functions to be run from command line
"notebook" function
*/

package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"strconv"
	"strings"
)

var workdir string = os.Getenv("HOME") + "/.config/dodo"

// UTITLITY FUNCTIONS
func itterate(call string, list int, data string) string {
	files, err := ioutil.ReadDir(workdir)
	if err != nil {
		fmt.Println("Error: ", err)
	}
	
	c := 1
	for _, file := range files {
		if call == "list" {
			fmt.Println(strconv.Itoa(c) + "\t" + file.Name())
		}

		if c == list {
			switch call {
				case "delete":
					os.Remove(workdir + "/" + file.Name())
					return ""

				case "rename":
					os.Rename(workdir + "/" + file.Name(), workdir + "/" + data)
					return ""

				case "edit":
					return file.Name()

			}
		}
		c++
	}
	
	return ""
}

func clearTerminal() {
    var cmd *exec.Cmd
    cmd = exec.Command("clear")
    cmd.Stdout = os.Stdout
    cmd.Run()
}

// MAIN LEVEL FUNCTIONS
func showHelp() {
	fmt.Printf(`l					show all todo lists
n (name)			new todo list : IF LIST ALREDY EXITS IT WILL BE DELETED WITHOUT CONFORMATION
d (number)			delete list (number)
r (number) (name)	rename a list to (name)
c					clear terminal
q					quit program
e (name)			switch to edit mode
	l						list all items
	n						create new item
	d (number)				delete the checked off lines or a specific number
	x (number)				check off line (number)
	r (number) (text)		replace line (number) with (text)
	c						clear terminal
	q						quit edit mode
`)
}

func newList(list string) {
	os.Create(workdir + "/" + list)
}

// using itterate
func showLists() {
	itterate("list", 0, "")
}

func deleteList(list int) {
	itterate("delete", list, "")
}

func renameList(list int, newName string) {
	itterate("rename", list, newName)
}

// EDIT FUNCTIONS
func showItems(file string) {
	f, err := os.Open(workdir + "/" + file)
	if err != nil {
		fmt.Println("Error opening list " + file)
		return
	}
	defer f.Close()

	sc := bufio.NewScanner(f)
	ln := 1
	for sc.Scan() {
		line := sc.Text()
		fmt.Println(strconv.Itoa(ln) + "\t" + line)
		ln++
	}

	if err := sc.Err(); err != nil {
		fmt.Println("Error reading list " + file)
		return
	}
}

func lineIterate(file string, item int, call string) {
	f, err := os.Open(file)
	if err != nil {
		fmt.Println("Error opening list " + file)
		return
	}
	defer f.Close()

	sc := bufio.NewScanner(f)
    lines := make([]string, 0)

	for sc.Scan() {
        lines = append(lines, sc.Text())
    }

    // Replace line
    if item < 1 || item > len(lines) {
        fmt.Println("Invalid line number")
		return
    }

	switch call {
		case "replace":
			fmt.Print("Enter new item: ")
			sc:= bufio.NewScanner(os.Stdin)
			sc.Scan()
			input := sc.Text()
			lines[item-1] = input
			
		case "check":
			lines[item-1] = "[x] " + lines[item-1]

		case "del":
			lines[item-1] = ""
		
		default:
			fmt.Println("Internal error.\nNot a valid call.")
			return
	}
	// Write modified content back to file
	output := strings.Join(lines, "\n")
	err = ioutil.WriteFile(file, []byte(output), 0644)
	if err != nil {
		fmt.Println("Cannot write file.")
		return
	}
}

func replaceItem(file string, item int) {
	lineIterate(file, item, "replace")
}

func checkItem(file string, item int) {
	lineIterate(file, item, "check")
}

func deleteItem(file string, item int) {
	lineIterate(file, item, "del")
}

func newItem(f string) {
	// Prompt user for input
    fmt.Print("Enter new item: ")
    sc:= bufio.NewScanner(os.Stdin)
    sc.Scan()
    input := sc.Text()

    // Open file for writing
    file, err := os.OpenFile(f, os.O_APPEND|os.O_WRONLY, 0644)
    defer file.Close()

    // Write input to file
    writer := bufio.NewWriter(file)
    _, err = writer.WriteString(input + "\n")
    if err != nil {
		fmt.Println("Error writing to file.")
        return
    }
    writer.Flush()
}

func editList(list int) {
	f := itterate("edit", list, "")
	file, err := os.Open(workdir + "/" + f)

	if err != nil {
		fmt.Println("List does not exist.")
		return
	}

	defer file.Close()

	for {
		var c, s, n string //command, string, newname

		fmt.Print("edit > ")
		fmt.Scanln(&c, &s, &n)

		i, _:= strconv.Atoi(s) //integer

		switch c {
			case "l":
				showItems(f)
			case "n":
				newItem(file.Name())
			case "x": 
				checkItem(file.Name(), i)
			case "r":
				replaceItem(file.Name(), i)
			case "d":
				deleteItem(file.Name(), i)
			case "q":
				mainLoop()
			case "h":
				showHelp()
			case "c":
				clearTerminal()
			default:
				fmt.Println("Not a valid command.")
		}
	}
}


func mainLoop() {
	for {
		var c, s, n string //command, string, newname

		fmt.Print("> ")
		fmt.Scanln(&c, &s, &n)

		i, _:= strconv.Atoi(s) //integer

		switch c {
			case "l":
				showLists()
			case "n":
				newList(s)
			case "e": 
				editList(i)
			case "d":
				deleteList(i)
			case "r":
				renameList(i, n)
			case "q":
				os.Exit(0)
			case "h":
				showHelp()
			case "c":
				clearTerminal()
			default:
				fmt.Println("Not a valid command.")
		}
	}
}

func main() {
	// Checking to see if workdir exists
	if _, err := os.Stat(os.Getenv(workdir)); err != nil {
		if os.IsNotExist(err) == true {
			if mkdirerr := os.Mkdir(workdir, os.ModePerm); mkdirerr == nil {
				fmt.Println(workdir + " does not exist. Creating...");
				fmt.Println(workdir + " made.")
			}
		}
	}
	
	mainLoop()
}
